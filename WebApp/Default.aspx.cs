﻿namespace WebApp
{
    using System;
    using System.Web.UI;

    public partial class _Default : Page
    {
        protected void HandleSave(object sender, EventArgs e)
        {
            string command1 = "UPDATE Sample SET";
            command1 += $" TimeSinceOC={this.row1_OC.Text}";
            command1 += $",TimeSinceOH={this.row1_OH.Text}";
            command1 += $" WHERE SampleId={this.row1_Id.Text}";

            ExecuteSql(command1);

            string command2 = "UPDATE Sample SET";
            command2 += $" TimeSinceOC={this.row2_OC.Text}";
            command2 += $",TimeSinceOH={this.row2_OH.Text}";
            command2 += $" WHERE SampleId={this.row2_Id.Text}";

            ExecuteSql(command2);

            string command3 = "UPDATE Sample SET";
            command3 += $" TimeSinceOC={this.row3_OC.Text}";
            command3 += $",TimeSinceOH={this.row3_OH.Text}";
            command3 += $" WHERE SampleId={this.row3_Id.Text}";

            ExecuteSql(command3);
        }

        private void ExecuteSql(string command)
        {
            Console.WriteLine(command);
        }
    }
}