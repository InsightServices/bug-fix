# Bug Fix

## Instructions
1. Clone this repository to your machine via SSH (~/Source/Repos)
2. Create a branch for your changes
3. Pay attention to the note and prerequisite sections below
4. Examine the code and leave comments answering the questions listed below
5. Commit your answers and push your branch to BitBucket

## Questions
1.	How would you approach debugging this error?  
2.	Do you see why the error may have occurred?
3.	What could we do to prevent this kind of error?

## Details
One of our lab techs showed us an error message on their screen.  The error message was:

> `An error has occurred. Error Description: (-2147217900) [Microsoft][ODBC SQL Server Driver][SQL Server]Incorrect syntax near '845'. SQL Statement:UPDATE Sample SET TimeSinceOH=62,845, TimeSinceOC=995, WHERE Sample.SampleId=123456789`

This error came from a screen in our system that allows lab techs to enter an oil sample into the database. The technicians are instructed to enter the information as it appears on the label.

The technician reported seeing the error when they click "Save".

## Database Structure
The data is being written into a table with the following schema _(Some columns omitted for brevity)_:
```
CREATE TABLE [dbo].[Sample](
  [SampleId] [int] NOT NULL,
  [TimeSinceOH] [int] NULL,
  [TimeSinceOC] [int] NULL,
  CONSTRAINT [PK_Sample] PRIMARY KEY CLUSTERED 
  (
    [SampleId] ASC
  )
```

## Note
This example project runs in Visual Studio 2017, but not Visual Studio Code.

## Prerequisite
After cloning, you may receive this error at runtime:

> Could not find a part of the path 'C:\Workspace\Project\Solution\Project\bin\roslyn\csc.exe'.

Run a NuGet restore on the solution, then run this command in the Package Manager Console to resolve the error:

`Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r`